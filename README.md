# Software Studio 2020 Spring
## Assignment 01 Web Canvas

### Scoring

| **Basic components**                             | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Basic control tools                              | 30%       | Y         |
| Text input                                       | 10%       | Y         |
| Cursor icon                                      | 10%       | Y         |
| Refresh button                                   | 10%       | Y         |

| **Advanced tools**                               | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Different brush shapes                           | 15%       | Y         |
| Un/Re-do button                                  | 10%       | Y         |
| Image tool                                       | 5%        | Y         |
| Download                                         | 5%        | Y         |

| **Other useful widgets**                         | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Name of widgets                                 | 1~5%     | Y         |


---

### How to use 

    我的CANVAS以Chrome打開為最佳，若使用其他瀏覽器，可能會有稍微的跑版
    
*   頁面外觀
    ![](https://i.imgur.com/JQRgffn.jpg)
    
        我的頁面上的ＭyCanvas會在刷新頁面後由上方進來，畫布會從左方進來，在頁面上的每個元
        素我都有給它們上陰影，這樣會看起來比較有立體感，按鈕hover時會放大且變色，能更清楚
        的看出現在點選的是什麼
    
*   畫筆
    ![](https://i.imgur.com/Gi56WH3.png)
    
        一開始進入頁面後點選畫筆即可開始作畫，default的顏色為黑色，但顏色是可以調整的，由
        上方的色盤即可改變按鈕顏色，另外粗細也是可以調整的，調整方法可由上方拉動來調整粗細
    
*   Refresh
    ![](https://i.imgur.com/9V5DXqz.png)
    
        點選refresh按鈕後即可將畫布清空

*   橡皮擦
    ![](https://i.imgur.com/QWTYjS3.png)
    
        點選橡皮擦後即可將畫布上不想要的部分擦掉，我將橡皮擦的大小調比較大，因為這樣能比較清楚的
        看出擦拭功能
    
*   Un/Redo
    ![](https://i.imgur.com/3AZ9ehc.png) ![](https://i.imgur.com/6nuynWr.png)
    
        點選undo可以返回上一步，redo則是可以進到下一步，且清空畫布後一樣可以返回，避免誤觸
        refresh導致圖消失
    
*   Download
    ![](https://i.imgur.com/gTeEUDH.png)
    
        Download可以將現在的畫布下載下來，存成png檔，之後可以使用upload上傳圖片
    
*   Upload
    ![](https://i.imgur.com/T2IBDuX.png)
    
        Upload可以將圖片上傳上去，並且這個圖片可以在畫布裡編輯(擦拭...)
  
*   Rectangle
    ![](https://i.imgur.com/b2XKYtf.png)

        點選這個按鈕後可以畫出一個空心的長方形，長方形的顏色和粗細可調整


*   Circle
    ![](https://i.imgur.com/4t6HzGE.png)

        點選這個按鈕後可以畫出一個空心的圓形，圓形的顏色和粗細可調整

*   Triangle
    ![](https://i.imgur.com/A229v53.png)
    
        點選這個按鈕後可以畫出一個空心的三角形，三角形的顏色和粗細可調整

*   Text
    ![](https://i.imgur.com/DaMRrHv.png)
    
        點選後點擊畫布即可輸入文字，文字的大小和字型皆可更改，且一次只能使用一個文字框，必須
        打完後才能使用下個文字框，打完後點一下螢幕文字就會上去（不是按Ｅnter）
         
*   Fill
    ![](https://i.imgur.com/shAaHa9.png)

        點選這個按鈕後，再去畫長方形、圓形和三角形，畫出來的圖形會變實心的，在點一次後畫的又會
        變成空心的，除此之外鼠標也會在空心實心間變換

*   Color
    ![](https://i.imgur.com/QMGruHF.png)
    
        點選可以叫出色盤來調顏色，文字圖形皆可調整

*   筆刷粗細
    ![](https://i.imgur.com/pWiCpoy.png)
    
        拉動來調整筆刷粗細，圖形也適用
        
*   Font-size Font-Family
    ![](https://i.imgur.com/Ytj5Yly.png)
    
        用來調整字體和字的大小    
        

> 順道一提，我的三角形往上拉時會是向上的三角形，下拉時會是倒三角形，與一般小畫家不同，因為我是在計算尖端頂點時，它的x座標是界於滑鼠起始位置和終點位置中間，y座標則與終點位置相同，故可畫出倒三角形
    
    
### Function description

*   Fill
   
        功能為畫出實心圖形，利用一個flag功能的fillshape來判斷現在要畫出實心還是空心圖形，同時
        利用此來變換鼠標圖案，在實心空心間來回切換

*   開頭

        剛開始畫布會先由右方滑進來，再過一下標題才會由上方滑落，同時標題MyCanvas會在紅黑之間
        快速變換看起來有種閃爍的感覺
        
### Gitlab page link

[https://107062309.gitlab.io/AS_01_WebCanvas](https://107062309.gitlab.io/AS_01_WebCanvas)

### Others (Optional)

    建議助教使用一般筆電來demo，若使用桌電，會看起來比較小
    
###### tags `Canvas`

<style>
table th{
    width: 100%;
}
</style