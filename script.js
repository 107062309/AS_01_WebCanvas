var brushSlide = document.getElementById("Brushsize")
let lWidth = 1;
let startX;
let startY;
let endX;
let endY;
let prevX;
let eraser = false;
let drawShape = "none";
let colorCur = "black";
let curFontF = "Arial";
let curFontS = "12px";
let fillShape = false;
let curShape;
let flag = 0;
let dataStore = [];
let redoStore = [];
var resetBtn = document.querySelector("#reset-button");
var eraseBtn = document.querySelector("#eraser-button");
var pencilBtn = document.querySelector("#pencil-button");
var colorChose = document.querySelector("#color-chose");
var fontFamilyChose = document.querySelector("#font-family");
var fontSizeChose = document.querySelector("#font-size");
var undoBtn = document.querySelector("#undo-button");
var redoBtn = document.querySelector("#redo-button");
var rectBtn = document.querySelector("#rect-button");
var circleBtn = document.querySelector("#circle-button");
var triangleBtn = document.querySelector("#triangle-button");
var textBtn = document.querySelector("#text-button");
var fillBtn = document.querySelector("#fill-button");
var downloadBtn = document.querySelector("#save-button");
var uploadBtn = document.querySelector("#uploadFile");
resetBtn.addEventListener("click", clear);
eraseBtn.addEventListener("click", erase);
pencilBtn.addEventListener("click", pencil);
undoBtn.addEventListener("click", undo);
redoBtn.addEventListener("click", redo);
rectBtn.addEventListener("click", rect);
circleBtn.addEventListener("click", circle);
triangleBtn.addEventListener("click", triangle);
textBtn.addEventListener("click", text);
fillBtn.addEventListener("click", fill);
downloadBtn.addEventListener("click", download);
uploadBtn.addEventListener("change", upload);
colorChose.addEventListener("change", color);
fontFamilyChose.addEventListener("change", fontfamilychose);
fontSizeChose.addEventListener("change", fontsizechose);

/////////////////////////////////////////////////////////////////////////////
//load window


window.onload =  function() {
    var canvas = document.querySelector("#Canvas");
    canvas.addEventListener("mousedown", mouseDown);
    canvas.addEventListener("mousemove", mouseMove);
    canvas.addEventListener("mouseup", mouseUp);
    canvas.addEventListener("click", addText);
    document.body.addEventListener("click", textPos);
    // canvas.addEventListener("keydown", keyDown);
}

/////////////////////////////////////////////////////////////////////////////
//mouse position

function getMousePos(canvas, e) {
    let rect = canvas.getBoundingClientRect();
    let pos = {
        x: Math.ceil((e.clientX - rect.left) / (rect.right - rect.left) * canvas.width),
        y: Math.ceil((e.clientY - rect.top) / (rect.bottom - rect.top) * canvas.height)
    };
    return pos;
}

function mouseDown(e) {
    var canvas = document.querySelector("#Canvas");
    this.enable = true; // draw or not
    this.ctx = this.getContext("2d");
    this.ctx.lineWidth = lWidth;
    this.ctx.strokeStyle = colorCur;
    this.ctx.lineCap = "round";
    this.ctx.lineJoin = "round";

    let position = getMousePos(this, e);
    startX = position.x;
    startY = position.y;
    prevX = startX;


    if (dataStore.length == 0) {
        this.firstDot = this.ctx.getImageData(0, 0, canvas.width, canvas.height);
        save(this.firstDot); // save data
    }

    var pos = this;
    this.offsetX = this.offsetLeft;
    this.offsetY = this.offsetTop;

    while (pos.offsetParent) {
        pos = pos.offsetParent;
        this.offsetX += pos.offsetLeft;
        this.offsetY += pos.offsetTop;
    }  //find the position of the canvas in the webpage

    this.ctx.beginPath();
    this.ctx.moveTo(e.pageX - this.offsetX, e.pageY - this.offsetY); //relative position to convas
}

function mouseMove(e) {
    if (this.enable && drawShape == "none" && flag == 1) {
        if (eraser) {
            this.ctx.globalCompositeOperation="destination-out";
            this.ctx.lineWidth = 30;
            // this.ctx.arc(e.pageX - this.offsetX, e.pageY - this.offsetY,8,0,Math.PI*2,false);
            // this.ctx.moveTo(e.pageX - this.offsetX, e.pageY - this.offsetY); //relative position to convas
            this.ctx.lineTo(e.pageX - this.offsetX, e.pageY - this.offsetY);
            this.ctx.stroke(); //draw
            // this.ctx.fill();
        }
        else {
            this.ctx.globalCompositeOperation="source-over";
            this.ctx.lineTo(e.pageX - this.offsetX, e.pageY - this.offsetY);
            this.ctx.stroke(); //draw
        }
    }
    if (this.enable && flag == 1)
    {
        let pos = getMousePos(this, e);
        endX = pos.x;
        endY = pos.y;
        if (drawShape != "none") {
            if (drawShape == "Rect")
                drawRect();
            else if (drawShape == "Circle")
                drawCircle();
            else if (drawShape == "Triangle")
                drawTriangle();
        }
        // }
    }
}

function mouseUp(e) {
    var canvas=document.querySelector('#Canvas');
    this.firstDot = this.ctx.getImageData(0, 0, canvas.width, canvas.height);
    save(this.firstDot); // save data
    this.enable = false; //finish drawing
    drawShape = curShape;
}

/////////////////////////////////////////////////////////////////////////////
//basic option

function clear() {
    var canvas=document.querySelector('#Canvas');
    var ctx=canvas.getContext("2d");
    ctx.clearRect(0,0,canvas.width,canvas.height);
    firstDot = ctx.getImageData(0, 0, canvas.width, canvas.height);
    save(firstDot); // save data
    clearprev = true;
    // flag = 1;
}

function erase() {
    drawText = false;
    eraser = true;
    drawShape = "none";
    curShape = "none";
    flag = 1;
    var canvas=document.querySelector('#Canvas');
    canvas.style.cursor = 'url("./picture/eraser.png"), default';
}

function pencil() {
    drawText = false;
    eraser = false;
    drawShape = "none";
    curShape = "none";
    flag = 1;  
    var canvas=document.querySelector('#Canvas');
    canvas.style.cursor = 'url("./picture/pencil.png"), default';
}

function color(e) {
    var canvas=document.querySelector('#Canvas');
    var ctx=canvas.getContext("2d");
    colorCur = e.target.value;
    // ctx.fillStyle = colorCur;
    ctx.strokeStyle = colorCur;
}

brushSlide.onchange = function(){
    lWidth = this.value;
};

/////////////////////////////////////////////////////////////////////////////
//re/undo

function save(data) {  //save data for undo
    // (dataStore.length === 10) && (dataStore.shift());
    dataStore.push(data);
    // clearStore.push(data);
    // console.log(dataStore.length);
    // console.log(clearStore.length);
}

function undo() {
    var canvas=document.querySelector('#Canvas');
    var ctx=canvas.getContext("2d");
    if(dataStore.length < 1) return false;
    redoStore.push(dataStore[dataStore.length - 1]);
    dataStore.pop();
    // clearStore.pop();
    ctx.putImageData(dataStore[dataStore.length - 1], 0, 0);
}

function redo() {
    var canvas=document.querySelector('#Canvas');
    var ctx=canvas.getContext("2d");
    if(redoStore.length < 1) return false;
    dataStore.push(redoStore[redoStore.length - 1]);
    ctx.putImageData(dataStore[dataStore.length - 1], 0, 0);
    redoStore.pop()
}

/////////////////////////////////////////////////////////////////////////////
//down/upload

function download() {
    var canvas=document.querySelector('#Canvas');
    let saveImg = document.createElement("a");
    let imgUrl = canvas.toDataURL("image/png");
    document.body.appendChild(saveImg);
    saveImg.href = imgUrl;
    saveImg.download = "Canvas" + (new Date).getTime();
    saveImg.click();
}

function DrawImage(src, x = 0, y = 0) {
    let img = new Image();
    var canvas=document.querySelector('#Canvas');
    var ctx=canvas.getContext("2d");
    ctx.globalCompositeOperation="source-over";
    canvas.style.cursor = "default";
    img.src = src;
    img.onload = function() {
      x = window.innerWidth / 2 - img.width / 2;
      y = window.innerHeight / 2 - img.height / 2;
      ctx.drawImage(img, x, y);
      firstDot = ctx.getImageData(0, 0, canvas.width, canvas.height);
      save(firstDot); // save data
    //   base64 = this.canvas.toDataURL();
    };
}

function upload(e) {
    writeText = false;
    eraser = false;
    flag = 0;
    let file = e.target.files[0];
    if (!file) return;
    let read = new FileReader();
    read.readAsDataURL(file);
    read.onload = () => {
        DrawImage(read.result);
    };
}

/////////////////////////////////////////////////////////////////////////////
//drawShape

function rect() {
    var canvas=document.querySelector('#Canvas');
    var ctx=canvas.getContext("2d");
    flag = 1;
    if (fillShape)
        canvas.style.cursor = 'url("./picture/filledrectangle.png"), default';
    else
        canvas.style.cursor = 'url("./picture/rectangle.png"), default';
    curShape = "Rect";
    drawShape = curShape;
    eraser = false;
    drawText = false;
    ctx.globalCompositeOperation="source-over";
}

function circle() {
    var canvas=document.querySelector('#Canvas');
    var ctx=canvas.getContext("2d");
    flag = 1;
    if (fillShape)
        canvas.style.cursor = 'url("./picture/filledcircle.png"), default';
    else
        canvas.style.cursor = 'url("./picture/circle.png"), default';
    curShape = "Circle";
    drawShape = curShape;
    eraser = false;
    drawText = false;
    ctx.globalCompositeOperation="source-over";
}

function triangle() {
    var canvas=document.querySelector('#Canvas');
    var ctx=canvas.getContext("2d");
    flag = 1;
    if (fillShape)
        canvas.style.cursor = 'url("./picture/filledtriangle.png"), default';
    else
        canvas.style.cursor = 'url("./picture/triangle.png"), default';
    curShape = "Triangle";
    drawShape = curShape;
    eraser = false;
    drawText = false;
    ctx.globalCompositeOperation="source-over";
}

function drawRect() {
    var canvas=document.querySelector('#Canvas');
    var ctx=canvas.getContext("2d");
    if (fillShape)
        canvas.style.cursor = 'url("./picture/filledrectangle.png"), default';
    else
        canvas.style.cursor = 'url("./picture/rectangle.png"), default';
    ctx.putImageData(dataStore[dataStore.length - 1], 0, 0);
    ctx.fillStyle = colorCur;
    ctx.strokeStyle = colorCur;
    // ctx.save();
    ctx.beginPath();
    ctx.rect(startX, startY, endX - startX, endY - startY);
    if (fillShape)
        ctx.fill();
    ctx.stroke();
    // ctx.clip();
}

function drawCircle() {
    var canvas=document.querySelector('#Canvas');
    var ctx=canvas.getContext("2d");
    if (fillShape)
        canvas.style.cursor = 'url("./picture/filledcircle.png"), default';
    else
        canvas.style.cursor = 'url("./picture/circle.png"), default';
    ctx.putImageData(dataStore[dataStore.length - 1], 0, 0);
    let radius = Math.sqrt((startX - endX) * (startX - endX) + (startY - endY) * (startY - endY))/2;
    ctx.fillStyle = colorCur;
    ctx.strokeStyle = colorCur;
    // ctx.save();
    ctx.beginPath();
    ctx.arc((startX + endX)/2, (startY + endY)/2, radius, 0, Math.PI * 2);
    if (fillShape)
        ctx.fill();
    ctx.stroke();
    // ctx.clip();
}

function drawTriangle() {
    var canvas=document.querySelector('#Canvas');
    var ctx=canvas.getContext("2d");
    if (fillShape)
        canvas.style.cursor = 'url("./picture/filledtriangle.png"), default';
    else
        canvas.style.cursor = 'url("./picture/triangle.png"), default';
    ctx.putImageData(dataStore[dataStore.length - 1], 0, 0);
    ctx.fillStyle = colorCur;
    ctx.strokeStyle = colorCur;
    // ctx.save();
    ctx.beginPath();
    ctx.moveTo(startX - (startX - endX)/2, startY);
    ctx.lineTo(startX, endY);
    ctx.lineTo(endX, endY);
    ctx.closePath();
    if (fillShape)
        ctx.fill();
    ctx.stroke();
    // ctx.clip();
}

/////////////////////////////////////////////////////////////////////////////
// text
var drawText = 0;
var input = 0;
var x = 0;
var y = 0;

function text() {
    var canvas=document.querySelector('#Canvas');
    var ctx=canvas.getContext("2d");
    drawText = 1;
    canvas.style.cursor = "text";
    flag = 0;
    eraser = false;
    ctx.globalCompositeOperation="source-over";
}

function addText(e) {
    if(input === 0 && drawText === 1){
        var div = $('<input id = "test"  type="text" />')
        .css({
            "left": e.pageX,
            "top": e.pageY, 
        })
        .appendTo(document.body);
        x = e.pageX;
        y = e.pageY;
        input = 1;
    }
}

function textPos() {
    if(input === 1 && document.getElementById("test").value != []){
        input = 0;
        drawText = 0;
        var canvas=document.querySelector('#Canvas');
        var ctx=canvas.getContext("2d");
        var inputData = document.getElementById("test").value;
        console.log(colorCur);
        ctx.font = curFontS + " " + curFontF;
        ctx.fillStyle = colorCur;
        ctx.fillText(inputData, x-17.5, y-141);
        var border = document.getElementById('test');
        border.remove();
        dataStore.push(ctx.getImageData(0, 0, canvas.width, canvas.height));
        // clearStore.push(ctx.getImageData(0, 0, canvas.width, canvas.height));
    }
}

function fontfamilychose() {
    curFontF = fontFamilyChose.value;
}

function fontsizechose() {
    curFontS = fontSizeChose.value;
}
/////////////////////////////////////////////////////////////////////////////
// fill color

function fill() {
    if (fillShape)
        fillShape = false;
    else
        fillShape = true;
}

